package com.example.alunos.meuslivros;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class LivroListAdapter extends RecyclerView.Adapter<LivroListAdapter.LivroViewHolder> {
    class  LivroViewHolder extends RecyclerView.ViewHolder{
        private final TextView tituloItemView;
        private final TextView autorItemView;
        private final TextView editoraItemView;
        private final ImageButton editaLivro;
        private final ImageButton deletaLivro;

        private LivroViewHolder(View itemView){
            super(itemView);
            tituloItemView = itemView.findViewById(R.id.titulo);
            autorItemView = itemView.findViewById(R.id.autor);
            editoraItemView = itemView.findViewById(R.id.editora);
            editaLivro = itemView.findViewById(R.id.editButton);
            deletaLivro = itemView.findViewById(R.id.deleteButton);

            editaLivro.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    if(listaLivros != null){
                        Livro current = listaLivros.get(getAdapterPosition());
                        Intent intent = new Intent(context, EditActivity.class);
                        intent.putExtra("livro", current);
                        ((Activity)context).startActivityForResult(intent, MainActivity.EDIT_LIVRO_ACTIVITY_REQUEST_CODE);
                    }
                }
            });

            deletaLivro.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(R.string.delete_dialog_title)
                            .setMessage(R.string.delete_dialog_message)
                            .setNegativeButton(R.string.label_cancel_delete, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    return;
                                }
                            })
                            .setPositiveButton(R.string.label_confirm_delete, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Livro current = listaLivros.get(getAdapterPosition());
                                    ((MainActivity) context).getLivroViewModel().delete(current);
                                }
                            });
                    builder.create().show();
                }
            });

        }
    }

    private final LayoutInflater mInflater;
    private List<Livro> listaLivros;
    private final Context context;

    LivroListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    @NonNull
    public LivroViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new LivroViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LivroViewHolder holder, int position){
        if(listaLivros != null){
            Livro current = listaLivros.get(position);
            holder.tituloItemView.setText(current.getTitulo());
            holder.autorItemView.setText(current.getAutor());
            holder.editoraItemView.setText(current.getEditora());
        }else{
            holder.tituloItemView.setText("Sem dados");
            holder.autorItemView.setText("Sem dados");
            holder.editoraItemView.setText("Sem dados");
        }


    }

    void setWords(List<Livro> livros){
        listaLivros = livros;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        if(listaLivros != null)
            return listaLivros.size();
        else return  0;
    }
}
