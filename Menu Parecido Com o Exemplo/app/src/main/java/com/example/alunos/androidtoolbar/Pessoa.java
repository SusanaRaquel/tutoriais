package com.example.alunos.androidtoolbar;

public class Pessoa {
    private String nome;
    private String email;

    public Pessoa (String t, String a, String d){
        this.nome = t;
        this.email = a;
    }

    public String getNome(){
        return nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }
}
