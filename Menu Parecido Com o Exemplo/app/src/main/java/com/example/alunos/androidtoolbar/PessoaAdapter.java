package com.example.alunos.androidtoolbar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.alunos.androidtoolbar.Pessoa;

import java.util.ArrayList;

public class PessoaAdapter extends RecyclerView.Adapter{

    Context context;
    ArrayList<Pessoa> lista;

    public PessoaAdapter(Context context, ArrayList<Pessoa> lista){
        this.context = context;
        this.lista = lista;
    }
}

@NonNull
@Override
public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int )
