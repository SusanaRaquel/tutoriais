package com.example.alunos.meuslivros;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class LivroRepository {
    private LivroDAO livroDAO;
    private LiveData<List<Livro>> listaLivro;

    LivroRepository(Application application){
        LivroRoomDatabase db = LivroRoomDatabase.getDatabase(application);
        livroDAO = db.livroDAO();
        listaLivro = livroDAO.getAllLivros();
    }

    LiveData<List<Livro>> getAllLivros(){
        return listaLivro;
    }

    public void insert (Livro Livro) {
        new insertAsyncTask(livroDAO).execute(Livro);
    }

    public void update(Livro livro) {
        new updateAsynctask(livroDAO).execute(livro);
    }

    public void delete(Livro livro) {
        new deleteAsyncTask(livroDAO).execute(livro);
    }

    private static class insertAsyncTask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDao;

        insertAsyncTask(LivroDAO dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected  Void doInBackground(final Livro... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateAsynctask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDao;

        private updateAsynctask(LivroDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Livro... params){
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }


    private class deleteAsyncTask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDao;
        public deleteAsyncTask(LivroDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Livro... livros){
            mAsyncTaskDao.delete(livros[0]);
            return null;
        }
    }
}

