package com.example.alunos.jogodeadivinha;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    Random rand = new Random();
    int num;
    int contador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        num = rand.nextInt(1000);
    }

    public void jogar(View v){
        EditText palpite = findViewById(R.id.texto);
        String valor = palpite.getText().toString();
        int valorConv = Integer.parseInt(valor);

        TextView msg = findViewById(R.id.resultado);

        if(valorConv == num){
            msg.setText(getResources().getString(R.string.acertou));
        }else if(valorConv < num){
            msg.setText(getResources().getString(R.string.baixo));
        }
        else {
            msg.setText(getResources().getString(R.string.alto));
        }
        contador++;



    }

    public void placarJogo(View v){
        Bundle bundle =  new Bundle();
        bundle.putInt("contador", contador);
        Intent i = new Intent(MainActivity.this, placar.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    public void recomecar(View v){
        num = rand.nextInt(1000);
        contador = 0;
        TextView msg = findViewById(R.id.texto);
        msg.setText("");
    }

}
