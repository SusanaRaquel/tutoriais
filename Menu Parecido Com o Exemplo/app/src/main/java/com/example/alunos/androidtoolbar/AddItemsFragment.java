package com.example.alunos.androidtoolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


public class AddItemsFragment extends Fragment {
    MainActivity activity;
    EditText nome;
    EditText email;
    Button salvar;
    ArrayList<Pessoa> lista;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_items_layout,
                container, false);
        activity = (MainActivity)getActivity();
        lista = activity.getLista();
        nome = v.findViewById(R.id.txtNome);
        email = v.findViewById(R.id.txtEmail);
        salvar = v.findViewById(R.id.btnSalvar);
        salvar.setOnClickListener((new View.OnClickListener(){
            @Override
            public void OnClick(View v) {
            if(nome.getText().toString().matches("")||
                    email.getText().toString().matches(""){
                Toast toast = Toast.makeText(activity.getApplicationContext(), "Existem campos vazios", Toast.LENGTH_SHORT);
                toast.show();
                return;
            }
        }

        return v;

    }
}
