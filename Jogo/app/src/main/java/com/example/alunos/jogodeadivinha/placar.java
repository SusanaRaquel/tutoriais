package com.example.alunos.jogodeadivinha;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class placar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placar);


        Intent intencao = getIntent();
        Bundle pacote = intencao.getExtras();

        if(pacote != null) {
            int tentativa = pacote.getInt("contador");
            SharedPreferences placar = getPreferences(Context.MODE_PRIVATE);
            Context context = getApplicationContext();
            int record = placar.getInt("record",1000);
            int placar0  = placar.getInt("placar0", 0);
            int placar1  = placar.getInt("placar1", 0);
            int placar2  = placar.getInt("placar2", 0);
            int placar3  = placar.getInt("placar3", 0);
            int placar4  = placar.getInt("placar4", 0);
            SharedPreferences.Editor editor = placar.edit();
            if( tentativa < record){
                editor.putInt("record",tentativa );
                record = tentativa;
            }
            placar4 = placar3;
            placar3 = placar2;
            placar2 = placar1;
            placar1 = placar0;
            placar0 = tentativa;

            editor.putInt("placar0",placar0 );
            editor.putInt("placar1",placar1 );
            editor.putInt("placar2",placar2 );
            editor.putInt("placar3",placar3 );
            editor.putInt("placar4",placar4 );
            editor.commit();

            TextView recorde = (TextView) findViewById(R.id.resultado);
            TextView Placar0 = (TextView) findViewById(R.id.Placar0);
            TextView Placar1 = (TextView) findViewById(R.id.Placar1);
            TextView Placar2 = (TextView) findViewById(R.id.Placar2);
            TextView Placar3 = (TextView) findViewById(R.id.Placar3);
            TextView Placar4 = (TextView) findViewById(R.id.Placar4);

            recorde.setText(Integer.toString(tentativa));
            Placar0.setText(Integer.toString(placar0));
            Placar1.setText(Integer.toString(placar1));
            Placar2.setText(Integer.toString(placar2));
            Placar3.setText(Integer.toString(placar3));
            Placar4.setText(Integer.toString(placar4));

        }

    }
}
